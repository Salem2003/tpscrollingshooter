﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float xLimit = 70;
    public float yLimit = 20;
    
    virtual public void Update()
    {
        CheckLimits();
    }

    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Void"))
        {
            Destroy(this.gameObject);
        }
    }

    internal virtual void CheckLimits() //se destruye al pasar los limites
    {
        if (this.transform.position.x > xLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.x < -xLimit && gameObject.CompareTag("EnemyProjectile"))
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.y > yLimit && gameObject.CompareTag("EnemyProjectile"))
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.y < -yLimit && gameObject.CompareTag("EnemyProjectile"))
        {
            Destroy(this.gameObject);
        }

    }

}
