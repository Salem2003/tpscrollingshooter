using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldEnemy : Controller_Enemy
{
    //public bool goingUp;

    public bool forward = true;

    private float timer = 1f;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    override public void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            rb.velocity = new Vector3(0, 9, 0);
            /*if (forward)
            {
                forward = false;
            }
            else
            {
                //goingUp = !goingUp;
                forward = true;
            }*/
            timer = 1f;
        }
        base.Update();
    }

    void FixedUpdate()
    {
        if (forward)
        {
            rb.AddForce(new Vector3(-1, 0, 0) * enemySpeed, ForceMode.Impulse);

            if (transform.position.x >= 20)
            {
                forward = false;
                transform.position = new Vector3(20, 9, 0);
            }
        }
        else
        {
            transform.position = new Vector3(20, 9, 0);
        }

    }
}
