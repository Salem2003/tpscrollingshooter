﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public float enemySpeed;

    public float xLimit;

    private float shootingCooldown;

    public GameObject enemyProjectile;

    public GameObject powerUp;

    public int puntos = 0;

    public int hp = 0;

    public Transform voidPower;

    void Start()
    {
        shootingCooldown = UnityEngine.Random.Range(3, 10);
        voidPower = GameObject.Find("Void").transform;
    }

    public virtual void Update()
    {
        shootingCooldown -= Time.deltaTime; //aparentemente, esto es una manera simplificada de hacer un ienumerator para el cooldown del disparo :)
        CheckLimits();
        ShootPlayer();

        if (Controller_Player.voidP)
        {
            transform.position = new Vector3(voidPower.transform.position.x, voidPower.transform.position.y, voidPower.transform.position.z) * Time.deltaTime;
        }
    }

    void ShootPlayer()
    {
        if (Controller_Player._Player != null) //el enemigo dispara al terminarse el cooldown--------------------
        {
            if (shootingCooldown <= 0)
            {
                Instantiate(enemyProjectile, transform.position, Quaternion.identity);
                shootingCooldown = UnityEngine.Random.Range(3, 10);
            }
        }
    }


    private void CheckLimits() //si el enemigo esta "fuera de la pantalla", lo elimino----------------------
    {
        if (this.transform.position.x < xLimit)
        {
            Destroy(gameObject);
        }
    }

    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            GeneratePowerUp();

            /*if (this.gameObject.CompareTag("Shield"))
            {
                //rebotan pero x algun motivo algunas no rebotan y no hacen daño al pj
                //collision.gameObject.GetComponent<Controller_Projectile>().projectileSpeed = -collision.gameObject.GetComponent<Controller_Projectile>().projectileSpeed;
                //collision.gameObject.tag = "EnemyProjectile";
            }
            else
            {*/
            Destroy(collision.gameObject);
            //}

            hp -= 5;
            if (hp <= 0)
            {
                Destroy(gameObject);
                Controller_Hud.points += puntos;
            }
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            if (!gameObject.CompareTag("Shield"))
            {
                GeneratePowerUp();

                collision.gameObject.GetComponent<Controller_Laser>().hits--;

                if (collision.gameObject.GetComponent<Controller_Laser>().hits == 0)
                {
                    Destroy(collision.gameObject);
                }

                hp -= 10;  //hacer q la cantidad de hit reducida dependa de la cantidad de hps del enemigo
                if (hp <= 0)
                {
                    Destroy(gameObject);
                    Controller_Hud.points += puntos;
                }
            }
            else
            {
                collision.gameObject.GetComponent<Controller_Laser>().hits = 0;

                hp -= 5;  //hacer q la cantidad de hit reducida dependa de la cantidad de hps del enemigo
                if (hp <= 0)
                {
                    Destroy(gameObject);
                    Controller_Hud.points += puntos;
                }
            }
            
        }
        if (collision.gameObject.CompareTag("Void"))
        {
            hp = 0;
            Destroy(gameObject);
            Controller_Hud.points += puntos;
        }
    }

    private void GeneratePowerUp()
    {
        int rnd = UnityEngine.Random.Range(0, 3); //random para instanciar (o no) un  powerup al matar un enemigo------------------------------

        if (rnd == 2)
        {
            Instantiate(powerUp, transform.position, Quaternion.identity); //quaternion.identity sirve para instanciar al objeto con una rotacion nula, es deciir 0, 0, 0 :)
        }
    }
}
