﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{

    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R)) //reinicio (practicamente) toda la escena
        {
            Controller_Player._Player.gameObject.SetActive(true);
            Controller_Player._Player.ReiniciarStats();
            Controller_Player._Player.gameObject.transform.position = Vector3.zero; //reposiciono al pj en la posicion 0, 0, 0--------------------------
            Controller_Hud.gameOver = false;
            //if (!Controller_Hud.gameOver)
            //{
                //Destroy(Controller_Player._Player.gameObject);
            //}

            //Instantiate(Controller_Player._Player.gameObject);
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
    }
}
